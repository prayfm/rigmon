const   config = require('./config.json'),
        TelegramBot = require('node-telegram-bot-api'),
        claymore = require('./claymore.js'),
        ewbfmonitor = require('./ewbf.js'),
        Poloniex = require('./poloniex'),
        moment = require('moment'),
        axios = require('axios'),
        bittrex = require('node-bittrex-api'),
        https = require('https');
        Poloniex.STRICT_SSL = false;

const   bot = new TelegramBot(config.token, {polling: true});        
        
var ison = new Array();
var oldon = new Array();
var algos = ["Ethash","Equihash","CryptoNight","ClayDual"],
    wtmal = ["","",""],
    suf   = ["MH/s","H/s","H/s","MH/s"];

var poloniex = new Poloniex(
        config.poloniex.key,
        config.poloniex.secret
    );
    
    bittrex.options({
        'apikey' : config.bittrex.key,
        'apisecret' : config.bittrex.secret
    });

setInterval(function(){
    a = ewbfmonitor.getData();
    b = claymore.getData();
    all = a.concat(b);
    any = false;
    msgtext = "<pre>";
    if(all.length>0){
        for (var j in all) {
            if(all[j].error == null) {
                ison[j] = true;
            } else {
                ison[j] = false;
            }
        }
        for (var j in ison) {
            if(ison[j] != oldon[j]) {
                if(ison[j]){
                    console.log(all[j].name+" online "+Date())
                    msgtext = msgtext + all[j].name + " is online\n";
                }
                else{
                    msgtext = msgtext + all[j].name + " is offline, e:" + all[j].error + "\n";
                    console.log(all[j].name+" offline "+Date());
                };
                any = true;   
            }
        }
        oldon = Object.assign({},ison);
    }
    msgtext = msgtext + "</pre>";
    if(any){
        bot.sendMessage(config.chatid,text=msgtext,{parse_mode : "HTML"});
    }
},config.miner_poll);

bot.onText(/\/id/, (msg) => {    
    bot.sendMessage(msg.chat.id,text="<pre>Your chat ID is: "+msg.chat.id+"</pre>",{parse_mode : "HTML"}); 
});

bot.onText(/\/help/, (msg) => {    
    bot.sendMessage(msg.chat.id,text="<pre>Available commands:\n/status, /detailed, /pools, /ip, /profit</pre>",{parse_mode : "HTML"});
});

bot.onText(/\/ip/, (msg) => {
    msgtext = "<pre>IP LIST\n";
    for(var f in all) {
        rig = all[f];
        msgtext = msgtext + rig.name + ": " + rig.host + " " + rig.comments + "\n";
    }
    msgtext = msgtext + "</pre>";
    bot.sendMessage(msg.chat.id,text=msgtext,{parse_mode : "HTML"});
});

bot.onText(/\/pools/, (msg) => {
    msgtext = "<pre>POOL LIST\n";
    for(var f in all) {
        rig = all[f];
        msgtext = msgtext + rig.name + ": " + rig.pools + " " + rig.ver + "\n";
    }
    msgtext = msgtext + "</pre>";
    bot.sendMessage(msg.chat.id,text=msgtext,{parse_mode : "HTML"});
});

bot.onText(/\/status/, (msg) => {    
    msgtext = "<pre>STATISTICS \n";
    rigs = Object.assign({},all);
    for(var f in rigs) {
        rig = rigs[f];
        msgtext = msgtext + f + ":" + rig.name + " | "
        if(rig.error === null) {
            time = moment.unix(rig.uptime);
            up = moment(time).toNow(true);
            if (rig.hash2 > 0) {
                msgtext = msgtext + (rig.hash/1000).toFixed(2) + " MH/s, " + (rig.hash2/1000).toFixed(2) + "MH/s, up:" + up + "\n";
            } else {
                if (rig.hash > 10000){
                msgtext = msgtext + (rig.hash/1000).toFixed(2) + " MH/s, up:" + up + "\n"; 
                } else{
                    msgtext = msgtext + rig.hash + " H/s, up:" + up + "\n";    
                }
            }
        } else {
            time = moment.unix(rig.last_seen);
            last = moment(time).fromNow();
            msgtext = msgtext + rig.error + ", last seen:" + last + "\n";
        }
    }
    msgtext = msgtext + "</pre>";
    bot.sendMessage(msg.chat.id,text=msgtext,{parse_mode : "HTML"});    
});

bot.onText(/\/profit/, (msg) => {
    rigs = Object.assign({},all);    
    msgtext = "<pre>PROFIT \n";
    power = [0,0,0,0];
    hash = [0,0,0,0];
    for(var f in rigs) {
        rig = rigs[f];     
        if(rig.error == null) {
            if (rig.algo == "eth"){
                hash[0] = hash[0] + rig.hash*1;
                hash[3] = hash[3] + rig.hash2*1;
                power[0] = power[0] + rig.power*1;
                wtmal[0] = "&eth=true&factor%5Beth_hr%5D=";
            } 
            if (rig.algo == "eq"){
                hash[1] = hash[1] + rig.hash*1;
                power[1] = power[1] + rig.power*1;
                wtmal[1] = "&eq=true&factor%5Beq_hr%5D=";
            }      
            if (rig.algo == "cn"){
                hash[2] = hash[2] + rig.hash*1;
                power[2] = power[2] + rig.power*1;
                wtmal[2] = "&cn=true&factor%5Bcn_hr%5D=";
            }      
        }
    }
    allpower = 0;
    for(var f in power){
        allpower = allpower+power[f];
    }
    allpower = allpower/1000;
    hash[0] = hash[0]/1000;
    hash[3] = hash[3]/1000;
    kwprice = config.electricity;
    e = config.currency_symbol;
    for(var f in algos){
        if(hash[f]>0){           
            msgtext = msgtext + algos[f] + ": " + hash[f].toFixed(2) + suf[f] + ", " + power[f] + "W, " + (hash[f]/power[f]).toFixed(2) + "\n"
        }
    }
    msgtext = msgtext + "Power: " + allpower.toFixed(2) + "kW\n";
    msgtext = msgtext + "Electricity cost:\nd:" + (allpower*kwprice*24).toFixed(2) + e + " w:" + (allpower*kwprice*24*7).toFixed(2) + e + " m:" + (allpower*kwprice*24*30).toFixed(2) + e + "\n";
    wtmpath = "https://whattomine.com/coins.json?utf8=%E2%9C%93";
    for(var f in wtmal){
        if(hash[f]>0){
            wtmpath = wtmpath + wtmal[f] + hash[f];
        }        
    };
    coinpath = "https://api.coinmarketcap.com/v1/ticker/bitcoin/?convert="+config.currency;
    let price = '';
    let profits = '';
    https.get(wtmpath, (res) => {
        res.on('data', (chunk) => {
            profits += chunk;
        });
        res.on('end', () => {
            profits = JSON.parse(profits);
            https.get(coinpath, (res) => {
                res.on('data', (chunk) => {
                    price += chunk;
                });
                res.on('end', () => {
                    ccurr = "price_" + config.currency;
                    price = JSON.parse(price);
                    btc = price[0][ccurr]; 
                    msgtext = msgtext + "Revenue:\n";
                    ethtext = "Ethash:\n";
                    eqtext = "Equihash:\n";
                    cntext = "CryptoNote:\n";
                    for (var key in profits["coins"]) {
                        var profit = profits["coins"][key];
                        if(profit.algorithm == "Ethash") ethtext = ethtext + profit.tag + " | d:" + (profit.btc_revenue24*btc).toFixed(2) + e + ", w:" + (profit.btc_revenue24*btc*7).toFixed(0) + e + ", m:" + (profit.btc_revenue24*btc*30).toFixed(0) + e + "\n";
                        if(profit.algorithm == "Equihash") eqtext = eqtext + profit.tag + " | d:" + (profit.btc_revenue24*btc).toFixed(2) + e + ", w:" + (profit.btc_revenue24*btc*7).toFixed(0) + e + ", m:" + (profit.btc_revenue24*btc*30).toFixed(0) + e + "\n";
                        if(profit.algorithm == "CryptoNote") cntext = cntext + profit.tag + " | d:" + (profit.btc_revenue24*btc).toFixed(2) + e + ", w:" + (profit.btc_revenue24*btc*7).toFixed(0) + e + ", m:" + (profit.btc_revenue24*btc*30).toFixed(0) + e + "\n";    
                    };
                    if (hash[0] > 0){
                        msgtext = msgtext + ethtext;
                        peth = profits.coins.Ethereum.btc_revenue24*1} else {peth=0};
                    if (hash[1] > 0){                        
                        msgtext = msgtext + eqtext;
                        peq = profits.coins.Zcash.btc_revenue24*1} else {peq=0};
                    if (hash[2] > 0){
                        msgtext = msgtext + cntext;
                        pecn = profits.coins.Monero.btc_revenue24*1} else {pecn=0};             
                    dayprofit = (pecn+peq+peth)*btc - allpower*kwprice*24;
                    msgtext = msgtext + "ZEC+ETH+XMR-Electricity:\nd:" + dayprofit.toFixed(2) + e + ", w:" + (dayprofit*7).toFixed(2) + e + "\nm:" + (dayprofit*30).toFixed(2) + e + ", y:" + (dayprofit*365).toFixed(2) + e + "</pre>";
                    bot.sendMessage(msg.chat.id,text=msgtext,{parse_mode : "HTML"});                    
                });
            }).on("error", (err) => {
                console.log("er: " + err.message);
                msgtext = "<pre>coinmarketcap.com err: " + err.message + "</pre>";
                bot.sendMessage(msg.chat.id,text=msgtext,{parse_mode : "HTML"});  
            });
        });
    }).on("error", (err) => {
        console.log("er: " + err.message);
        msgtext = "<pre>whattomine.com err: " + err.message + "</pre>";
        bot.sendMessage(msg.chat.id,text=msgtext,{parse_mode : "HTML"});
    });    
});

bot.onText(/\/detailed/, (msg) => {
    rigs = Object.assign({},all);
    msgtext = "<pre>DETAILED STATISTICS \n";
    for(var f in rigs) {
        rig=rigs[f];
        msgtext = msgtext + rig.name;
        if(rig.error == null) {
            msgtext = msgtext + " | " + (rig.hash/rig.power).toFixed(2) + "H/W" + "\n";
            for(var j in rig.gpu){
                gpu = rig.gpu[j];
                if (rig.ver.slice(-3) == "EWB"){
                    msgtext = msgtext + "  " + gpu.hash + " H/s " + gpu.temps + "c " + gpu.power + "W " + (gpu.hash/gpu.power).toFixed(2) + "H/W\n"}
                if (rig.ver.slice(-3) == "ETH" ){
                    msgtext = msgtext + "  " + (gpu.hash/1000).toFixed(2) + " MH/s " + gpu.temps + "c " + gpu.fan + "% " + "\n";
                }                      
                if (rig.ver.slice(-3) == "ZEC" || rig.ver.slice(-3) == "XMR" ){
                    msgtext = msgtext + "  " + gpu.hash + " H/s " + gpu.temps + "c " + gpu.fan + "% " + "\n";
                }                         
            }            
        } 
        else {
           msgtext = msgtext + " | OFF\n"; 
        }
    };
    msgtext = msgtext + "</pre>";
    bot.sendMessage(msg.chat.id,text=msgtext,{parse_mode : "HTML"})  
});

bot.onText(/\/balances/, (msg) => {    
        var bal;
        bittrex.getbalances( function( data, err ) {
            if (err) {
                return console.error(err);
            };
            bittdata = data.result;
            console.log('bit ok')
        });
    Poloniex.STRICT_SSL = false;
        poloniex.returnCompleteBalances(function(err, data){
            if (err){
                console.log('ERROR', err);
                return;
            }            
            bal = data;  
            console.log('POLO ok');   
        });
        coinmarket = "https://api.coinmarketcap.com/v1/ticker/bitcoin/?convert=EUR";
        axios.get(coinmarket)
        .then(function (response){
            price = response.data;
            console.log('coinmarketcap ok');
        })
        .catch(function (error){
            price = error;
        });    
        setTimeout(function(){
            console.log(bal);
            msgtext = "<pre> POLONIEX:\n";        
            btc = price[0].price_eur;  
            total = 0;
            for (var key in bal) {
                var coin = bal[key].btcValue;
                var name = String(key);
                if (coin > 0){
                    msgtext = msgtext + name + ": " + (coin*btc).toFixed(2) + "€\n";
                }
                total = total + coin*1;
            };
            total = total * btc;
            msgtext = msgtext + "Viso:" + total.toFixed(2) + "€\n\n BITTREX:\n";
            for (var i in bittdata) {
                msgtext = msgtext + bittdata[i].Currency + ": " + bittdata[i].Balance.toFixed(8)+"\n";
            };
            msgtext = msgtext + "</pre>";
            bot.sendMessage(msg.chat.id,text=msgtext,{parse_mode : "HTML"});
        }, 5000)
    });
    