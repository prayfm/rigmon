var config = require('./config.json');
var net = require('net');

var ewbf = [];
ewbf.json = [];


config.ewbf.forEach(function(item, i, arr) {

    // settings
    var m = ewbf[i] = {};
    var c = config.ewbf[i];
    var j = ewbf.json[i];

    m.name = c.name;
    m.host = c.host;
    m.port = c.port;
    m.poll = (typeof c.poll !== 'undefined') ? c.poll : config.miner_poll;
    m.timeout = (typeof c.timeout !== 'undefined') ? c.timeout : config.miner_timeout;

    function hostname() {
        return c.hostname ? c.hostname : (m.host + ':' + m.port);
    }

    // stats
    m.reqCnt = 0;
    m.rspCnt = 0;

    // it was never seen and never found good yet
    c.last_seen = null;
    c.last_good = null;

    // socket
    m.socket = new net.Socket()

    .on('connect', function() {
        var req = '{"id":0,"jsonrpc":"2.0","method":"getstat"}';
        ++m.reqCnt;
        m.socket.write(req + '\n');
        m.socket.setTimeout(m.timeout);
    })

    .on('timeout', function() {
        m.socket.destroy();
        ewbf.json[i] = {
            "name"       : m.name,
            "host"       : hostname(),
            "uptime"     : "",
            "pools"      : "",
            "ver"        : "",
            "comments"   : c.comments,
            "offline"    : c.offline,
            "warning"    : null,
            "error"      : 'Error: no response',
            "power"      : "",
            "gpu"        : [],
            "last_seen"  : c.last_seen ? c.last_seen : 0
        };
    })

    .on('data', function(data) {
        ++m.rspCnt;
        c.last_seen = Math.floor(Date.now() / 1000);
        m.socket.setTimeout(0);
        var d = JSON.parse(data);
        ntotal = 0;
        ngpu = [];
        for (j in d.result) {
            ntotal = ntotal + d.result[j].speed_sps;
            ngpu[j] ={
                "hash" : d.result[j].speed_sps,
                "hash2" : -1,
                "temps" : d.result[j].temperature,
                "fan"   : -1,
                "power" : d.result[j].gpu_power_usage
            }
        }
        nerror = null;
        if(ntotal<1){
            nerror = "0 HASH";
        }
        ewbf.json[i] = {
            "name"       : m.name,
            "host"       : hostname(),
            "uptime"     : d.start_time,
            "pools"      : d.current_server,
            "ver"        : "EWB",
            "comments"   : c.comments,
            "offline"    : c.offline,
            "power"      : c.power,
            "error"      : nerror,
            "gpu"        : ngpu,
            "coin"       : c.coin,
            "algo"       : c.algo,
            "hash"       : ntotal
        };
    })

    .on('close', function() {
        setTimeout(poll, m.poll);
    })

    .on('error', function(e) {
        ewbf.json[i] = {
            "name"       : m.name,
            "host"       : hostname(),
            "uptime"     : "",
            "pools"      : "",
            "ver"        : "EWB",
            "comments"   : c.comments,
            "offline"    : c.offline,
            "warning"    : null,
            "error"      : e.code,
            "power"      : "",
            "gpu"        : [],
            "last_seen"  : c.last_seen ? c.last_seen : 0
        };
    });

    function poll() {
        m.socket.connect(m.port, m.host);
    };

    if ((typeof c.offline === 'undefined') || !c.offline) {
        poll();
    } else {
        ewbf.json[i] = {
            "name"       : m.name,
            "host"       : hostname(),
            "uptime"     : "",
            "pools"      : "",
            "ver"        : "EWB",
            "comments"   : c.comments,
            "offline"    : c.offline,
            "error"      : null,
            "power"      : "",
            "gpu"        : []
        };
    }
});

module.exports = {
    getData: function () {
        return ewbf.json;
    }
}
// --------------- /REQUESTER ---------------